import cv2
import glob
import re
import os
import shutil
import numpy as np
from utils import img_glob


def remove_red(image):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV_FULL)
    h = hsv[:, :, 0]
    s = hsv[:, :, 1]

    mask = np.zeros(h.shape, dtype=np.uint8) + 255
    mask[((h < 15) | (h > 244)) & (s > 40)] = 0
    mask = cv2.erode(mask, np.ones((3, 3), np.uint8), iterations=1)

    top = int(2 * image.shape[0] / 3)
    left = int(image.shape[1] / 2)
    mask[:top] = 255
    mask[:, :left] = 255

    img_masked = image.copy()
    img_masked[(mask == 0)] = (255, 255, 255)
    return img_masked, mask

def remove_red_images(imgs_dir, output_dir):
    img_path_list = img_glob(imgs_dir)
    os.makedirs(output_dir, exist_ok=True)
    for img_path in img_path_list[20000:]:
        try:
            print(img_path)
            img = cv2.imread(img_path)
            img_masked, mask = remove_red(img)
            top = int(2 * img.shape[0] / 3)
            left = int(img.shape[1] / 2)
            img_masked[top:, left:] = cv2.morphologyEx(img_masked[top:, left:], cv2.MORPH_CLOSE, np.ones((3, 3)))
            output_path = output_dir + img_path.split('/')[-1]
            cv2.imwrite(output_path, img_masked)
        except:
            print(img_path, 'error')


img_path_dir = '/Users/masayaiwasaki/Downloads/insite_cut_nolabel_sorted/2/kanjiname/'
output_dir = 'remove_red/'
remove_red_images(img_path_dir, output_dir)

'''
img = cv2.imread(img_path)
img_masked, mask = remove_red(img)
mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
print(img.shape, mask.shape)
co_img = cv2.vconcat([img, img_masked, mask])
cv2.imwrite('temp.png', co_img)
'''

