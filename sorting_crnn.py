# coding=utf-8
from keras.models import load_model
import argparse, os, sys, glob, cv2, h5py, shutil
import numpy as np
from utils import img_glob, normalize_text, _resize_image_keeping_aspect_ratio
import json
from keras import backend as K
import Levenshtein


sys.path.append(os.path.join(os.popen("git rev-parse --show-toplevel").read().strip(), 'CNN', 'utils'))


def extract_line(img, margin_h=5, margin_w=3, thread=0.01):
    def remove_region(char_region, thre=2):
        norm_char_region = []
        for region in char_region:
            if not (region[1] - region[0]) < thre:
                norm_char_region.append(region)
        return norm_char_region

    def cut_image(img, margin, thread, is_wide=True):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        gray = 255 - gray
        if is_wide:
            gray = gray.transpose()

        hist = [sum(temp) for temp in gray]
        hist_max, hist_min = max(hist), min(hist)

        if hist_max == 0:
            return np.zeros((64, 64, 3), dtype=np.uint8) + 255

        hist = [(h - hist_min) / hist_max for h in hist]

        char_region = []
        start, is_back = 0, True
        for (i, h) in enumerate(hist):
            if is_back and h > thread:
                start = i
                is_back = False
            if not is_back and h < thread:
                char_region.append((start, i))
                is_back = True
        if len(char_region) == 0:
            char_region.append((start, len(hist) - 1))
        elif not char_region[-1][0] == start:
            char_region.append((start, len(hist) - 1))
        char_region = remove_region(char_region, 3)

        if len(char_region) == 0:
            return img

        if not is_wide:
            max_area = 0
            for region in char_region:
                area = (region[1] - region[0])
                if area > max_area:
                    max_area = area
                    cut_point = region
            cut_point = (max(cut_point[0] - margin, 0), min(cut_point[1] + margin, len(hist)))
            img = img[cut_point[0]:cut_point[1], :]
        else:
            cut_point = (max(char_region[0][0] - margin, 0), min(char_region[-1][1] + margin, len(hist)))
            img = img[:, cut_point[0]:cut_point[1]]
        return img

    img = cut_image(img, margin_h, thread, is_wide=False)
    img = cut_image(img, margin_w, thread, is_wide=True)
    return img


def load_answer(image_path):
    json_file_path = os.path.splitext(image_path)[0] + '.json'

    if os.path.exists(json_file_path):
        with open(json_file_path, 'r') as f:
            json_data = json.load(f)
            if 'answer' in json_data:
                answer_string = json_data[u'answer']
            elif 'verify_string' in json_data:
                answer_string = json_data[u'verify_string']
            else:
                print('No answer!!!!!!')
                answer_string = ''
    else:
        answer_string = '.'.join(image_path.split('/')[-1].split('_')[-1].split('.')[:-1])
    return normalize_text(answer_string)


def norm_wide_undefine1(img, constant_input_shape=(None, 64, None, 1)):
    max_width = 4096

    img = extract_line(img)
    shape = (min(max_width, max(1, constant_input_shape[1] * img.shape[1] // img.shape[0])),
             constant_input_shape[1])
    img = cv2.resize(img, shape, interpolation=cv2.INTER_CUBIC)

    img = np.invert(img)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = img / 255
    img = np.expand_dims(img, axis=0)
    img = np.expand_dims(img, axis=3)
    return img


def norm_wide_undefine2(img, constant_input_shape=(None, 64, None, 1)):
    max_width = 4096

    img = extract_line(img)
    shape = (min(max_width, max(1, constant_input_shape[1] * img.shape[1] // img.shape[0])),
             constant_input_shape[1])
    img = cv2.resize(img, shape, interpolation=cv2.INTER_CUBIC)

    img = np.invert(img)
    img = img.transpose(1, 0, 2)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = img / 255
    img = np.expand_dims(img, axis=0)
    img = np.expand_dims(img, axis=3)
    return img


def norm(img, constant_input_shape=(None, 64, 1024, 1)):
    img = _resize_image_keeping_aspect_ratio(img, constant_input_shape=constant_input_shape)
    img = np.invert(img)
    img = img.transpose(1, 0, 2)
    img = cv2.resize(img, (constant_input_shape[1], constant_input_shape[2]))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = img / 255
    img = np.expand_dims(img, axis=0)
    img = np.expand_dims(img, axis=3)
    return img


def norm_batch1(images, constant_input_shape=(None, 64, 1024, 1)):
    imgs = np.array([]).reshape(0, constant_input_shape[1], constant_input_shape[2], constant_input_shape[3])
    for img in images:
        if img is None:
            img = np.zeros((64, 512, 3), dtype=np.uint8)
        else:
            img = _resize_image_keeping_aspect_ratio(img, constant_input_shape=constant_input_shape)
        img = np.invert(img)
        img = img.transpose(1, 0, 2)
        img = cv2.resize(img, (constant_input_shape[1], constant_input_shape[2]))
        img = img / 255
        img = np.expand_dims(img, axis=3)
        img = np.expand_dims(img, axis=0)
        imgs = np.append(imgs, img, axis=0)
    return imgs


def norm_batch2(images, constant_input_shape=(None, 64, 1024, 1)):
    imgs = np.array([]).reshape(0, constant_input_shape[2], constant_input_shape[1], constant_input_shape[3])
    for img in images:
        if img is None:
            img = np.zeros((64, 512, 3), dtype=np.uint8)
        else:
            img = _resize_image_keeping_aspect_ratio(img, constant_input_shape=constant_input_shape)
        img = np.invert(img)
        img = img.transpose(1, 0, 2)
        img = cv2.resize(img, (constant_input_shape[1], constant_input_shape[2]))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = img / 255
        img = np.expand_dims(img, axis=3)
        img = np.expand_dims(img, axis=0)
        imgs = np.append(imgs, img, axis=0)
    return imgs


def norm_batch3(images, constant_input_shape=(None, 64, 3072, 3)):
    imgs = np.array([]).reshape(0, constant_input_shape[1], constant_input_shape[2], constant_input_shape[3])
    for img in images:
        if img is None:
            img = np.zeros((64, 512, 3), dtype=np.uint8)
        else:
            img = _resize_image_keeping_aspect_ratio(img, constant_input_shape=constant_input_shape)
        img = np.expand_dims(img, axis=0)
        imgs = np.append(imgs, img, axis=0)
    return imgs


def output_log(log_file_name, results):
    f = open(log_file_name, 'w')
    for result in results:
        prefix = '○' if result['answer'] == result['text'] else '×'
        f.write(prefix + '\t' + result['answer'] + '\t' + result['text'] + '\t' + result['filename'] + '\n')
    f.close()


def predict(model_type, model_path, predict_datasets_path, leven_thre=0.2, batch_size=16, start=0, end=0, levendis_thre=2,
            sort_flag=True):
    # classes load
    with h5py.File(model_path, 'r') as h5file:
        classes = [s.decode('utf8') for s in np.asarray(h5file[os.path.join('ai_inside', 'classes')].value).flatten()]

    norm_function = {'name': norm_batch2, 'address': norm_batch2, 'phone': norm_batch2,
                     'tegaki_katakana_name': norm_batch1, 'all': norm_batch3}

    # model load
    model = load_model(model_path)

    output_path = predict_datasets_path + '/cannot_read/'
    output_path2 = predict_datasets_path + '/sus_read/'
    os.makedirs(output_path, exist_ok=True)
    os.makedirs(output_path2, exist_ok=True)

    # get target directory list
    img_path_list = img_glob(predict_datasets_path)
    img_path_list = img_path_list[start:end]
    count = 1
    results = []
    levens = 0
    perfects = 0

    for index in range(0, len(img_path_list), batch_size):
        imgs = [cv2.imread(image_path) for image_path in img_path_list[index:index+batch_size]]
        imgs = norm_function[model_type](imgs)

        out = model.predict(imgs)
        input_rnn_length = len(out[0])
        input_rnn_length_list = [input_rnn_length for _ in range(len(imgs))]
        decodes = K.ctc_decode(out, input_length=input_rnn_length_list)[0]
        labels = K.get_value(decodes[0])
        texts = []
        for label in labels:
            text = ''.join([classes[t] for t in label])
            text = text.replace(' ', '')
            texts.append(text)

        for (image_path, text) in zip(img_path_list[index:index+batch_size], texts):
            answer = load_answer(image_path)
            leven_score = Levenshtein.ratio(answer, text)
            leven_dis = Levenshtein.distance(answer, text)
            is_delate = leven_score < leven_thre
            if sort_flag:
                try:
                    if is_delate:
                        shutil.move(image_path, output_path)
                    if leven_dis >= levendis_thre:
                        shutil.move(image_path, output_path2)
                except:
                    print('error')
            print(count, '/', len(img_path_list), is_delate, leven_score, image_path.split('/')[-1], text)
            results.append({'filename': image_path.split('/')[-1], 'text': text, 'answer': answer})
            levens += leven_score
            perfects += 1 if answer == text else 0
            count += 1
    os.makedirs('result/', exist_ok=True)
    log_file_name = 'result/{0}_{1}_acc_{2:.5f}_leven_{3:.5f}.txt'.format(
        model_type, predict_datasets_path.split('/')[-2], perfects / len(img_path_list), levens / len(img_path_list))
    output_log(log_file_name, results)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--modeltype', type=str, default='all')
    parser.add_argument('--modelfile', type=str, default='/Users/masayaiwasaki/source/release/OCR_Engine_all.aiinside')
    parser.add_argument('--image_path', type=str, default='image/')
    parser.add_argument('--batch_size', type=int, default=16)
    parser.add_argument('--leven_thre', type=int, default=3)
    parser.add_argument('--sort_flag', type=bool, default=True)
    parser.add_argument('--start', type=int, default=0)
    parser.add_argument('--end', type=int, default=10000)

    args = parser.parse_args()
    model_type = args.modeltype
    model_path = args.modelfile
    predict_datasets_path = args.image_path
    batch_size = args.batch_size
    leven_thre = args.leven_thre
    sort_flag = args.sort_flag
    start = args.start
    end = args.end

    predict(model_type, model_path, predict_datasets_path, batch_size=batch_size, start=start, end=end,
            levendis_thre=leven_thre, sort_flag=sort_flag)
