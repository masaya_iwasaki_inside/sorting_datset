# coding=utf-8
from __future__ import division
import glob
import os
import re
import cv2
import numpy as np
import unicodedata
import Levenshtein
import json
import uuid
import random
import math
import shutil
import redis
import scipy.stats as stats
from keras.models import Model
from keras.layers import concatenate, Lambda
from scipy.ndimage.interpolation import map_coordinates
from scipy.ndimage.filters import gaussian_filter
import tensorflow as tf
BACKGROUND_PATH = os.getenv('BACKGROUND_PATH', 'background')


# def encode_strings_to_labels(strings, classes):
#     labels = []
#     for st in strings:
#         lb = [classes.index(t) for t in st]
#         labels.append(lb)
#     return labels

def imageprocessing(image, funcs):
    for func in funcs:
        func(image)
    return image


def decode_labels_to_strings(labels, classes):
    strings = []
    for lb in labels:
        st = [classes[t] for t in lb]
        strings.append(st)
    return strings


def show_inputs(inputs, classes, mode='r'):
    imgs = inputs[0][0]
    labels = inputs[0][1]
    strings = decode_labels_to_strings(labels, classes)
    for img, st in zip(imgs, strings):
        img = img.transpose(1, 0, 2)
        word = u''.join(st)
        if mode == 'r':
            print(word)
            cv2.imshow("image", img)
            cv2.waitKey(0)
        if mode == 'w':
            cv2.imwrite(word, img)
    cv2.destroyAllWindows()


def show_img(img):
    cv2.imshow('image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def gaussian_mono_noise(img, mean=0.0, std=1.0, astype=np.uint8):
    """
    Apply gaussian noise.
    """
    x = np.random.normal(mean, std, img.shape[:2]+(1,))
    noisy_img = img + x
    noisy_img = noisy_img.astype(astype)

    return cv2.addWeighted(img, 1, noisy_img, 1, 0.0)


def affine_transform(img, deg=15.):
    """
    Apply affine transformation.
    """
    h, w, _ = img.shape
    center = tuple(np.array([int(w/2), int(h/2)]))
    size = tuple(np.array([w, h]))
    angle = random.uniform(0, deg)
    scale = 1
    matrix = cv2.getRotationMatrix2D(center, angle, scale)
    affine_matrix = np.float32(matrix)
    return cv2.warpAffine(img, affine_matrix, size, flags=cv2.INTER_LINEAR)


def image_with_rust(img):
    severity = np.random.uniform(0, 1)
    blur = gaussian_filter(np.random.randn(*img.shape[:2]+(1,)) * severity, 1)
    img_speck = (img * blur)
    img_speck[img_speck > 1] = 1
    img_speck[img_speck <= 0] = 0
    return img_speck


def bounding_rect(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    x, y, w, h = cv2.boundingRect(thresh)
    return x, y, w, h


def dilate(img):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))
    img = cv2.dilate(img, kernel, iterations=1)
    return img


def upperscript(metachar, dataset_path, size):
    base, upper = metachar.split('^')
    base_path = os.path.join(dataset_path, hex(ord(base)))
    files = img_glob(base_path)
    base_img = cv2.imread(random.choice(files))
    base_img = resize_img(base_img, size)
    upper_path = os.path.join(dataset_path, hex(ord(upper)))
    files = img_glob(upper_path)
    upper_img = cv2.imread(random.choice(files))
    upper_img = dilate(upper_img)
    upper_img = resize_img(upper_img, size)
    h, w, _ = tuple([int(x/3) for x in upper_img.shape])
    upper_img = cv2.resize(upper_img, (w, h))
    blank_img = base_img.copy()
    blank_img[:] = 0
    blank_img[:len(upper_img[0]), -len(upper_img[1]):] = upper_img
    return cv2.addWeighted(base_img, 1, blank_img, 1, 0.0)


def subscript(metachar, dataset_path, size):
    base, sub = metachar.split('_')
    base_path = os.path.join(dataset_path, hex(ord(base)))
    files = img_glob(base_path)
    base_img = cv2.imread(random.choice(files))
    base_img = resize_img(base_img, size)
    sub_path = os.path.join(dataset_path, hex(ord(sub)))
    files = img_glob(sub_path)
    sub_img = cv2.imread(random.choice(files))
    sub_img = dilate(sub_img)
    sub_img = resize_img(sub_img, size)
    h, w, _ = tuple([int(x/3) for x in sub_img.shape])
    sub_img = cv2.resize(sub_img, (w, h))
    blank_img = base_img.copy()
    blank_img[:] = 0
    blank_img[-len(sub_img[0]):, -len(sub_img[1]):] = sub_img
    return cv2.addWeighted(base_img, 1, blank_img, 1, 0.0)


def zoom_image(image, axis='y'):
    """
    Remove the spaces around the character.
    """
    x, y, w, h = bounding_rect(image)
    y1, y2, x1, x2 = y, y + h, x, x + h
    if h < w:
        y1 = int(math.floor(max(0, y - (w - h) / 2)))
        y2 = y1 + w
    elif w < h:
        x1 = int(math.floor(max(0, x - (h - w) / 2)))
        x2 = x1 + h
    if axis == 'y':
        return image[y1: y2, :]
    elif axis == 'x':
        return image[:, x1: x2]
    elif axis == 's':
        return image[:y2, x1: x2]
    else:
        return image[y1: y2, x1: x2]


def elastic_transform(image, alpha, sigma, random_state=None):
    """Elastic deformation of images as described in [Simard2003]_.
    .. [Simard2003] Simard, Steinkraus and Platt, "Best Practices for
       Convolutional Neural Networks applied to Visual Document Analysis", in
       Proc. of the International Conference on Document Analysis and
       Recognition, 2003.
    """
    if random_state is None:
        random_state = np.random.RandomState(None)

    shape = image.shape
    dx = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma, mode="constant",
                         cval=0) * alpha
    dy = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma, mode="constant",
                         cval=0) * alpha

    x, y = np.meshgrid(np.arange(shape[0]), np.arange(shape[1]), indexing='ij')
    indices = np.reshape(x + dx, (-1, 1)), np.reshape(y + dy, (-1, 1))
    return map_coordinates(image, indices, order=1).reshape(shape)


def img_glob(path):
    ext = '.*\.(jpg|png|bmp)'
    path = os.path.join(path, '*')
    files = glob.glob(path)
    files = [f for f in files if re.search(ext, f, re.IGNORECASE)]
    return files


def json_glob(path):
    ext = '.*\.(json)'
    path = os.path.join(path, '*')
    files = glob.glob(path)
    files = [f for f in files if re.search(ext, f, re.IGNORECASE)]
    return files


def get_batch_img(path, batch_size, input_shape, cut):
    files = img_glob(path)
    files = random.sample(files, batch_size)
    imgs = [cv2.imread(f, cv2.IMREAD_COLOR) for f in files]
    imgs = reshape_imgs(imgs, input_shape)
    files = [f.split(cut)[-1] for f in files]
    files = [os.path.splitext(f)[0] for f in files]
    strings = list(map(lambda f: unicodedata.normalize('NFKC', f), files))
    return imgs, strings


def file_to_img(path):
    files = img_glob(path)
    imgs = [cv2.imread(f, cv2.IMREAD_COLOR) for f in files]
    return imgs


def json_to_str(path):
    files = json_glob(path)
    jsons = [open(f.decode('utf-8'), 'r') for f in files]
    ans = []
    for j in jsons:
        with j as f:
            ans.append(json.load(f)[u'answer'])
    return ans


def file_to_name(path, cut):
    files = img_glob(path)
    files = [f.split(cut)[-1] for f in files]
    files = [os.path.splitext(f)[0] for f in files]
    fnames = list(map(lambda f: unicodedata.normalize('NFKC', f), files))
    return fnames


def rename_file(path, cut):
    files = img_glob(path)
    fnames = file_to_name(path, cut)
    newdir = 'tmp'
    fnames = [uuid.uuid4().hex + u'_' + f + u'.png' for f in fnames]
    fnames = [os.path.join(newdir, f) for f in fnames]
    [shutil.copy(old, new) for old, new in zip(files, fnames)]
    return fnames


def resize_img(img, hsize):
    h, w = img.shape[:2]
    resize_h = h / h * hsize
    resize_w = w / h * hsize
    resized_img = cv2.resize(img, (int(resize_w), int(resize_h)))
    return resized_img


def reshape_imgs(imgs, shape):
    return [cv2.resize(img, (shape[1], shape[0])) for img in imgs]


def move_imgs(from_parent_path, to_parent_path, ratio_samples=0.05):
    assert ratio_samples < 1
    dirs = os.listdir(from_parent_path)
    for d in dirs:
        from_path = os.path.join(from_parent_path, d)
        to_path = os.path.join(to_parent_path, d)
        if not os.path.isdir(from_path):
            continue
        if not os.path.isdir(to_path):
            os.mkdir(to_path)
        move_files(from_path, to_path, ratio_samples)


def move_files(from_path, to_path, ratio_samples):
    files = img_glob(from_path)
    n = int(len(files) * ratio_samples)
    if n == 0:
        return None
    else:
        for f in random.sample(files, n):
            name = os.path.basename(f)
            g = os.path.join(to_path, name)
            shutil.move(f, g)


def reverse_color(imgs):
    return [255 - img for img in imgs]


def add_background(img):
    files = img_glob('background')
    fname = np.random.choice(files)
    bg = cv2.imread(fname, cv2.IMREAD_COLOR)
    bg = 255 - bg
    bg = random_cutting(bg)
    h, w, _ = img.shape
    bg = cv2.resize(bg, (w, h))
    img = cv2.addWeighted(img, 1, bg, 0.25, 0.0)
    return img


def verbose_ctc_seq(output_softmax, classes):
    for argmax in np.argmax(output_softmax, axis=2):
        for seq in list(zip(argmax, ''.join([classes[x] for x in argmax]))):
            print(seq)


def superposition(chr_img, str_img, intersect=True):
    h_str, w_str, ch_str = str_img.shape
    chr_img = resize_img(chr_img, h_str)
    h_chr, w_chr, ch_chr = chr_img.shape
    if intersect:
        w_pad = int(np.random.normal(0, w_chr/3))
        w_pad = 0 if abs(w_pad) > min(w_chr/3, w_str) else w_pad
        tmp_str_img = np.zeros((h_str, w_str + w_chr + w_pad, ch_str), dtype=np.uint8)
        tmp_chr_img = tmp_str_img.copy()
        tmp_str_img[:, :w_str, :] = str_img
        tmp_chr_img[:, -w_chr:, :] = chr_img
        str_img = cv2.addWeighted(tmp_str_img, 1, tmp_chr_img, 1, 0.0)
        return str_img
    else:
        return cv2.hconcat([str_img, chr_img])


def random_padding(img, grayscale = 0):
    height, width, ch = img.shape
    h = int(height/4)
    w = int(width/4)
    y_pad = np.random.randint(0, h+1)
    y_start = np.random.randint(0, y_pad+1)
    x_pad = np.random.randint(0, w+1)
    x_start = np.random.randint(0, x_pad+1)
    white_space = np.ndarray((height + y_pad, width + x_pad, ch), np.uint8)
    white_space[:] = grayscale
    y_min = y_start
    x_min = x_start
    y_max = height + y_min
    x_max = width + x_min
    white_space[y_min:y_max, x_min:x_max, :] = img
    return white_space


def random_cutting(img, grayscale=0):
    height, width, ch = img.shape
    h1 = int(height/8)
    h2 = int(height/8)
    y_start = np.random.randint(0, h1+1)
    y_stop = height - np.random.randint(0, h2+1)
    return img[y_start:y_stop, :]


def delete_blank_imgs(imgs):
    return [delete_blank(img) for img in imgs]


def delete_blank(img, ratio=0.75, axis='x'):
    h, w, _ = img.shape
    img_org = img.copy()
    img[int(ratio * h):] = 255
    img[:int(h-ratio*h)] = 255
    img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    ret, img_thresh = cv2.threshold(
        img_gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    comp = cv2.connectedComponentsWithStats(img_thresh)
    labelnum, labelimg, contours, GoCs = comp
    ball = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (25, 25))
    img_dilateText = cv2.morphologyEx(
        labelimg.astype(np.uint8), cv2.MORPH_DILATE, ball)
    comp = cv2.connectedComponentsWithStats(img_dilateText)
    labelnum, labelimg, contours, GoCs = comp
    data = []
    for i in list(np.argsort(contours.T[0]))[1:]:
        x_min = contours[i, 0]
        x_max = contours[i, 0] + contours[i, 2]
        y_min = contours[i, 1]
        y_max = contours[i, 1] + contours[i, 3]
        if axis == 'x':
            parts = img_org[:, x_min:x_max]
        else:
            parts = img_org[y_min:y_max, x_min:x_max]
        data.append(parts)
        img = cv2.hconcat(data)
    return img


def mget_texts(batch_size):
    conn = redis.StrictRedis(host=REDIS_HOST, port=6379)
    num_list = range(NUM_READLINES)
    num_list = random.sample(num_list, batch_size)
    texts = conn.mget(num_list)
    texts = [line for line in texts if line]
    return texts


def pkl_load(path):
    with open(path, 'rb') as f:
        r = redis.Redis(host=REDIS_HOST, port=6379, db=0)
        r.set('tree', f)


def redis_set(data):
    r = redis.Redis(host=REDIS_HOST, port=6379, db=0)
    r.set('tree', data)


def redis_get():
    r = redis.Redis(host=REDIS_HOST, port=6379, db=0)
    return r.get('tree')


def mset_texts():
    with open(NAME_DICT) as f:
        lines = f.readlines()
        r = redis.StrictRedis(host=REDIS_HOST, port=6379)
        lines = [line.strip('\n') for line in lines[:NUM_READLINES]]
        r.mset(lines)


def evaluate(classes, model, inputs):
    classes = classes + [' ']
    output_strings = model.predict(inputs)
    input_labels = inputs[0][1]
    assert len(output_strings) == len(input_labels)
    batch_size = len(input_labels)
    score = 0
    for i in range(batch_size):
        answer_token = ''.join([classes[t] for t in input_labels[i]])
        answer_token = answer_token.strip()
        decode_token = output_strings[i].strip()
        edit_distance = Levenshtein.distance(decode_token, answer_token)
        score += abs(len(answer_token) - edit_distance) / len(answer_token)
    return score / batch_size


def perfect_match_evaluate(classes, model, inputs):
    classes = classes + [' ']
    output_strings = model.predict(inputs)
    input_labels = inputs[0][1]
    assert len(output_strings) == len(input_labels)
    batch_size = len(input_labels)
    score = 0
    for i in range(batch_size):
        answer_token = ''.join([classes[t] for t in input_labels[i]])
        answer_token = answer_token.strip()
        decode_token = output_strings[i].strip()
        print(decode_token, answer_token)
        if decode_token == answer_token:
            score += 1
    return score / batch_size


def write_ans_img(inputs, imgs, strings, model):
    answers = model.predict(inputs)
    for ans, st, img in zip(answers, strings, imgs):
        if st == ans:
            cv2.imwrite('result/correct/image_{}_{}.png'.format(ans, st), img)
        else:
            cv2.imwrite('result/incorrect/image_{}_{}.png'.format(ans, st), img)


def make_parallel(model, gpu_count):
    def get_slice(data, idx, parts):
        shape = tf.shape(data)
        size = tf.concat([shape[:1] // parts, shape[1:]], axis=0)
        stride = tf.concat([shape[:1] // parts, shape[1:] * 0], axis=0)
        start = stride * idx
        return tf.slice(data, start, size)

    outputs_all = []
    for i in range(len(model.outputs)):
        outputs_all.append([])

    # Place a copy of the model on each GPU, each getting a slice of the batch
    for i in range(gpu_count):
        with tf.device('/gpu:%d' % i):
            with tf.name_scope('tower_%d' % i) as _:

                inputs = []
                # Slice each input into a piece for processing on this GPU
                for x in model.inputs:
                    input_shape = tuple(x.get_shape().as_list())[1:]
                    slice_n = Lambda(get_slice, output_shape=input_shape,
                                     arguments={'idx': i, 'parts': gpu_count})(x)
                    inputs.append(slice_n)

                outputs = model(inputs)

                if not isinstance(outputs, list):
                    outputs = [outputs]

                # Save all the outputs for merging back together later
                for l in range(len(outputs)):
                    outputs_all[l].append(outputs[l])

    # merge outputs on CPU
    with tf.device('/cpu:0'):
        merged = []
        for outputs in outputs_all:
            merged.append(concatenate(outputs, axis=0))

    return Model(inputs=model.inputs, outputs=merged)


def get_answer_from_png_path(png_file_path, from_json=True, remove_blank=True):
    if from_json:
        json_file_path = os.path.splitext(png_file_path)[0] + '.json'
        with open(json_file_path, 'r') as f:
            ans = json.load(f)[u'answer']
    else:
        ans = os.path.splitext(os.path.basename(png_file_path))[0].split('_')[-1]

    return normalize_text(ans, remove_blank=remove_blank)


def normalize_text(text, remove_blank=True):
    text = unicodedata.normalize('NFKC', text).replace('−', '-')
    if remove_blank:
        text = text.replace(' ', '')
    return text


def random_font_family(dataset_path):
    dir_path = random.choice(glob.glob(os.path.join(dataset_path, '0x*')))
    return random.choice(img_glob(dir_path)).split('/')[-1].split('.')[0]


def _resize_image_keeping_aspect_ratio(img, remove_top_bottom_as_well=True, constant_input_shape=None):
    # FIXME:batch support
    if img.shape[0] * img.shape[1] <= 0:
        return img

    # remove the redundant space on both of right and left side
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    x, y, w, h = cv2.boundingRect(np.invert(thresh))
    if remove_top_bottom_as_well:
        if w > 0 and h > 0:
            img = img[y: y + h, x: x + w, :]
    else:
        if w > 0:
            img = img[:, x: x + w, :]

    # change the height to the target one
    _, input_h, input_w, input_ch = constant_input_shape
    img = cv2.resize(img, (max(1, input_h * img.shape[1] // img.shape[0]), input_h),
                     interpolation=cv2.INTER_CUBIC)

    # make the image shrink the width if it is still long
    if input_w < img.shape[1]:
        img = cv2.resize(img, (input_w, input_h),
                         interpolation=cv2.INTER_CUBIC)

    # otherwise, add blank space to the right so that the size of the image can be the given one
    elif input_w >= img.shape[1]:
        img = _set_margin(img, margins=(0, input_w - img.shape[1], 0, 0))

    return img


def _set_margin(img, margins=(10, 10, 10, 10), background_color=255):
    canvas = np.zeros((img.shape[0] + margins[0] + margins[2],
                       img.shape[1] + margins[1] + margins[3], img.shape[-1]), np.uint8) + background_color
    y1, y2 = margins[0], margins[0] + img.shape[0]
    x1, x2 = margins[3], margins[3] + img.shape[1]
    canvas[y1: y2, x1: x2, :] = img
    return canvas


def resize_image_keeping_aspect_ratio(src_image, target_shape, remove_top_bottom_as_well=False):
    if src_image.shape[0] * src_image.shape[1] <= 0:
        return src_image

    # first, remove the redundant space around the image
    if len(src_image.shape) == 2 or src_image.shape[2] == 1:
        thresh = cv2.threshold(src_image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        x, y, w, h = cv2.boundingRect(255 - thresh)
        if remove_top_bottom_as_well:
            src_image = src_image[y: y + h, x: x + w]
        else:
            src_image = src_image[:, x: x + w]
    else:
        gray_image = cv2.cvtColor(src_image, cv2.COLOR_BGR2GRAY)
        thresh = cv2.threshold(gray_image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        x, y, w, h = cv2.boundingRect(255 - thresh)
        if remove_top_bottom_as_well:
            src_image = src_image[y: y + h, x: x + w, :]
        else:
            src_image = src_image[:, x: x + w, :]

    # change the height to the target one
    src_image = cv2.resize(src_image,
                           (max(1, src_image.shape[1] * target_shape[0] // src_image.shape[0]),
                            target_shape[0]), interpolation=cv2.INTER_CUBIC)

    # make the image shrink the width if it is still long
    if target_shape[1] < src_image.shape[1]:
        src_image = cv2.resize(src_image, (target_shape[1], target_shape[0]),
                               interpolation=cv2.INTER_CUBIC)

    # otherwise, add blank space to the right so that the size of the image can be the given one
    elif src_image.shape[1] < target_shape[1]:
        src_image = set_margin(src_image, margins=(0, target_shape[1] - src_image.shape[1], 0, 0))

    return src_image


def normalize_image(src_image, target_shape):
    # inverse blank and white and map [0, 255] to [0, 1]
    return (255 - resize_image_keeping_aspect_ratio(src_image, target_shape,
                                                    remove_top_bottom_as_well=True)) / 255.0


def denormalize_image(src_image):
    return (255.0 - 255.0 * src_image).astype(np.uint8)


def lower2upper(text):
    text = text.replace(u'ァ', u'ア')
    text = text.replace(u'ィ', u'イ')
    text = text.replace(u'ゥ', u'ウ')
    text = text.replace(u'ェ', u'エ')
    text = text.replace(u'ォ', u'オ')
    text = text.replace(u'ッ', u'ツ')
    text = text.replace(u'ャ', u'ヤ')
    text = text.replace(u'ュ', u'ユ')
    text = text.replace(u'ョ', u'ヨ')
    return text


def merge_images(source_image, target_image, top, left, operator=np.minimum):
    _top = min(top + source_image.shape[0], target_image.shape[0])
    _left = min(left + source_image.shape[1], target_image.shape[1])
    if top < _top and left < _left:
        target_image[top: _top, left: _left] = operator(target_image[top: _top, left: _left],
                                                        source_image[0: _top - top, 0: _left - left])

    return target_image


def set_margin(src_image, margins=(10, 10, 10, 10), background_color=255):

    canvas = np.zeros((src_image.shape[0] + margins[0] + margins[2],
                       src_image.shape[1] + margins[1] + margins[3]), np.uint8) + background_color
    y1, y2 = margins[0], margins[0] + src_image.shape[0]
    x1, x2 = margins[3], margins[3] + src_image.shape[1]
    canvas[y1: y2, x1: x2] = src_image
    return canvas


def truncated_norm(lower, upper, mu, sigma, to_int=False):
    value = stats.truncnorm((lower - mu) / sigma, (upper - mu) / sigma, loc=mu, scale=sigma).rvs()
    return int(round(value)) if to_int else value


def chip_char(src_image, thickness):
    mid = int(src_image.shape[0] / 2)
    matrix = 255 - np.zeros(src_image.shape[:2], dtype=np.uint8)
    cv2.line(matrix, (mid, 0), (mid, src_image.shape[1]), 0, thickness)
    matrix = rotate_and_shift(matrix, random.randint(0, 180),
                              random.randint(-mid, mid), random.randint(-mid, mid))
    matrix = matrix / 255
    return (src_image * matrix).astype(np.uint8)


def rotate_and_shift(src_image, rad, x, y):

    src_image = 255 - src_image
    size = tuple(np.array([src_image.shape[1], src_image.shape[0]]))
    rad = rad * np.pi / 180.0
    matrix = [
        [np.cos(rad), -1 * np.sin(rad), x],
        [np.sin(rad), np.cos(rad), y]
    ]
    affine_matrix = np.float32(matrix)
    return 255 - cv2.warpAffine(src_image, affine_matrix, size, flags=cv2.INTER_LINEAR)


def random_warp_perspective(src_image):
    rows, cols = src_image.shape[0:2]
    pts1 = np.float32([[0, 0], [rows, 0], [0, cols], [rows, cols]])
    pts2 = np.float32([
        [0, 0],
        [truncated_norm(rows / 3, rows, rows, rows / 6), 0],
        [0, truncated_norm(cols / 3, cols, cols, cols / 6)],
        [
            truncated_norm(rows / 3, rows, rows, rows / 6),
            truncated_norm(cols / 3, cols, cols, cols / 6)
        ]
    ])
    return cv2.warpPerspective(src_image, cv2.getPerspectiveTransform(pts1, pts2), (cols, rows))
