from utils import img_glob
import random
import os
import shutil

src_img_dir = '/Users/masayaiwasaki/Downloads/temp_insite/insite_cut_labeling/done/bankbranch_insite/'
out_img_dir = 'output/'

is_copy = False

os.makedirs(out_img_dir, exist_ok=True)

sampling_num = 300

src_img_path_list = img_glob(src_img_dir)
sample_list = random.sample(src_img_path_list, sampling_num)
for s in sample_list:
    print(s)
    if is_copy:
        shutil.copy(s, out_img_dir)
    else:
        shutil.move(s, out_img_dir)


