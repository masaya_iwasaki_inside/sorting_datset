# Fixme:This is temp code
from keras.models import load_model
import argparse, os, sys, glob, cv2, h5py, shutil
import numpy as np
from utils import img_glob, _resize_image_keeping_aspect_ratio
import json


sys.path.append(os.path.join(os.popen("git rev-parse --show-toplevel").read().strip(), 'CNN', 'utils'))


def norm_batch1(images, constant_input_shape=(None, 64, 1024, 1)):
    imgs = np.array([]).reshape(0, constant_input_shape[1], constant_input_shape[2], constant_input_shape[3])
    for img in images:
        if img is None:
            img = np.zeros(constant_input_shape[1:], dtype=np.uint8) + 255
            img = np.expand_dims(img, axis=0)
            imgs = np.append(imgs, img, axis=0)
            continue
        img = np.invert(img)
        img = _resize_image_keeping_aspect_ratio(img, constant_input_shape=constant_input_shape)
        img = cv2.resize(img, (constant_input_shape[2], constant_input_shape[1]))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = np.expand_dims(img, axis=3)
        img = np.expand_dims(img, axis=0)
        imgs = np.append(imgs, img, axis=0)
    return imgs


def blank_predict(model, predict_datasets_path, batch_size=2):
    # classes load
    with h5py.File(model_path, 'r') as h5file:
        classes = [s.decode('utf8') for s in np.asarray(h5file[os.path.join('ai_inside', 'classes')].value).flatten()]

    output_path = {'entry_do_not': predict_datasets_path + '/entry_do_not/',
                   'entry_can_not': predict_datasets_path + '/entry_can_not/',
                   'empty': predict_datasets_path + '/empty/',
                   'asta': predict_datasets_path + '/asta/',
                   }

    for out in output_path:
        os.makedirs(output_path[out], exist_ok=True)

    # get target directory list
    img_path_list = img_glob(predict_datasets_path)
    count = 1
    for index in range(0, len(img_path_list), batch_size):
        imgs = [cv2.imread(image_path) for image_path in img_path_list[index:index+batch_size]]
        imgs = norm_batch1(imgs, constant_input_shape=model.input_shape)

        output_softmax = model.predict(imgs)
        predict_results = [classes[out.argmax()] for out in output_softmax]

        for (image_path, predict_result) in zip(img_path_list[index:index+batch_size], predict_results):
            label = image_path.split('_')[-1].replace('.png', '')
            if '●' in label:
                shutil.move(image_path, output_path['asta'])
            elif not predict_result == 'written':
                shutil.move(image_path, output_path[predict_result])
            print(count, '/', len(img_path_list), predict_result, image_path.split('/')[-1])
            count += 1


if __name__=='__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--modelfile', type=str, default='/Users/masayaiwasaki/source/release/OCR_Engine_blank.aiinside')
    parser.add_argument('--image_path', type=str, default='image/')
    parser.add_argument('--batch_size', type=int, default=16)

    args = parser.parse_args()
    model_path = args.modelfile
    predict_datasets_path = args.image_path
    batch_size = args.batch_size

    # model load
    model = load_model(model_path)

    blank_predict(model, predict_datasets_path, batch_size=batch_size)

