from functools import cmp_to_key
from skimage.morphology import label as bwlabel
import cv2
import numpy as np
from utils import img_glob
import os
from keras.models import load_model
import keras.backend as K
import shutil
import argparse


class MultilinesBaseRecognizer():
    def __init__(self, modelfile, lexiconfile=None, detection_modelfile=None):
        self.modelfile = modelfile
        self.lexiconfile = lexiconfile
        self.detection_modelfile = detection_modelfile
        self.model = None
        self.classes = None
        self.constant_input_shape = (None, 64, None, 1)
        self.input_ratio = 16

    def load(self):
        self.detection_model = load_model(self.detection_modelfile, compile=False,
                                          custom_objects={'custom_softmax': self.detection_softmax})

    @staticmethod
    def detection_softmax(x):
        ndim = K.ndim(x)
        if ndim == 2:
            return K.softmax(x)
        elif ndim == 3:
            e = K.exp(x - K.max(x, axis=-1, keepdims=True))
            s = K.sum(e, axis=-1, keepdims=True)
            return e / s
        elif ndim == 4:
            e = K.exp(x - K.max(x, axis=1, keepdims=True))
            s = K.sum(e, axis=1, keepdims=True)
            return e / s
        else:
            raise Exception('Cannot apply softmax to a tensor that is not 2D or 3D. ' +
                            'Here, ndim=' + str(ndim))

    def detect_lines(self, model, image, likelihood=0.3, min_size=16):
        image_h, image_w = image.shape[0:2]
        if image_h * image_w <= 0:
            return [], [], []

        th_tensor = self._prepare_input(image, 'multiple')
        if th_tensor.mean() < 0:
            th_tensor = -th_tensor

        res_map = self._decode_image(model, th_tensor, image.shape[:2])
        bbox_lut, res_map = self._from_res_map_to_bbox(res_map)
        boxes = []
        for index, (top, bot, left, right) in enumerate(bbox_lut['bounding_box']):
            if likelihood < bbox_lut['proba'][
                index] and min_size <= bot - top and min_size <= right - left:
                boxes.append((top, bot, left, right))

        boxes = self._merge_boxes(boxes)

        if len(boxes) == 0:
            return [], res_map, []

        boxes = sorted(boxes, key=cmp_to_key(self._compare_boxes))

        # make it a little bit larger
        for index, (top, bot, left, right) in enumerate(boxes):
            top = max(0, top - 5)
            bot = min(image_h, bot + 5)
            left = int(max(0, right - 1.3 * (right - left)))
            right = int(min(image_w, left + 1.3 * (right - left)))
            boxes[index] = (top, bot, left, right)

        return boxes, res_map, bbox_lut['proba']

    def _merge_boxes(self, boxes):
        for i in range(len(boxes)):
            for j in range(i + 1, len(boxes)):
                top1, bot1, left1, right1 = boxes[i]
                top2, bot2, left2, right2 = boxes[j]
                h1 = bot1 - top1
                h2 = bot2 - top2
                dup_h = min(bot1, bot2) - max(top1, top2)
                # see if these two boxes need to be merged
                if h1 * 2 / 3.0 < dup_h or h2 * 2 / 3.0 < dup_h:
                    boxes[i] = (
                    min(top1, top2), max(bot1, bot2), min(left1, left2), max(right1, right2))
                    del boxes[j]
                    return self._merge_boxes(boxes)
        return boxes

    def _relax_wrt_border(self, raw_bbox, height, width, border_perc=.16):
        # load bbox
        top, bot, left, right = raw_bbox
        # compute box width and height
        box_w, box_h = right - left + 1, bot - top + 1
        # compute border width
        d = np.ceil(min(box_w, box_h) * (1. / (1 - border_perc * 2) - 1) * 0.5)
        # relax according to border info
        left, right = max(0, left - d), min(right + d, width)
        top, bot = max(0, top - d), min(bot + d, height)
        return [top, bot, left, right]

    def _from_res_map_to_bbox(self, res_map, th_size=8, th_prob=0.25, border_perc=.16):
        height, width = res_map.shape[:2]
        labels = res_map.argmax(axis=-1)
        text = labels == 2
        bwtext, nb_regs = bwlabel(text, return_num=True)
        lut = {'bounding_box': [], 'proba': []}
        for reg_id in range(1, nb_regs + 1):
            row_idx, col_idx = np.nonzero(bwtext == reg_id)
            # get four corners
            left, right = col_idx.min(), col_idx.max() + 1
            top, bot = row_idx.min(), row_idx.max() + 1
            # relax w.r.t. border
            bbox = self._relax_wrt_border([top, bot, left, right], height, width, border_perc)
            by0, by1, bx0, bx1 = bbox
            bh, bw = by1 - by0 + 1, bx1 - bx0 + 1
            # estimate text proba
            proba = np.median(res_map[top:bot, left:right, 2])
            if (proba >= th_prob) and (min(bh, bw) >= th_size):
                lut['bounding_box'].append([top, bot, left, right])
                lut['proba'].append(float(proba))
        return lut, res_map

    def _prepare_input(self, image_array, res_type='multiple'):
        """
        Prepare input image array to tensor
        """
        if res_type == 'single':
            multiple_of_x = 8
        else:  # multiple
            multiple_of_x = 96

        # determine padding patterns
        h, w = image_array.shape[:2]
        pad_h = (h // multiple_of_x * multiple_of_x - h) % multiple_of_x
        pad_w = (w // multiple_of_x * multiple_of_x - w) % multiple_of_x
        # pad image to make sure the new dimension is a multiple of ${multiple_of_X}
        image_pad = np.pad(image_array, ([0, pad_h], [0, pad_w], [0, 0]),
                           mode='symmetric')
        # convert image uint8 array to theano float32 tensor
        th_tensor = np.rollaxis(image_pad.astype(np.float32), 2, 0)
        th_tensor = np.expand_dims(th_tensor, 0) / 255. - 0.5
        return th_tensor

    def _decode_image(self, model, th_tensor, output_shape):
        """
        Decode a PPT image using an existing model
        """
        h, w = output_shape
        res_map = model.predict(th_tensor)
        res_map = np.rollaxis(res_map[0], 0, 3)
        res_map = res_map[:h, :w]
        return res_map

    def _compare_boxes(self, x, y):
        t1, b1, l1, r1 = x
        t2, b2, l2, r2 = y
        t = max(t1, t2)
        b = min(b1, b2)
        if t <= b and ((r2 - l2) * 3 // 5 + l2 < l1):
            return 1
        elif t <= b and ((r1 - l1) * 3 // 5 + l1 < l2):
            return -1
        else:
            return x[0] - y[0]

    def norm(self, img_dir):
        image_path_list = img_glob(img_dir)
        output = img_dir + 'trash/'
        os.makedirs(output, exist_ok=True)
        detection_likelihood = 0
        detection_min_size = 16
        count = 0
        for image_path in image_path_list:
            # image_path = 'image/20170418200111823_0044_畑中　覚.png'
            image = cv2.imread(image_path)
            boxes, res_map, prob = self.detect_lines(self.detection_model, image,
                                                     likelihood=detection_likelihood,
                                                     min_size=detection_min_size)

            is_gomi = self.gomi_detecter(boxes, res_map)
            if is_gomi:
                # print(boxes)
                shutil.move(image_path, output)

            print(count, '/', len(image_path_list), is_gomi, image_path.split('/')[-1])
            count += 1
            '''
            for box in boxes:
                cv2.rectangle(image, (box[2], box[0]), (box[3], box[2]), color=(255, 0, 0), thickness=1, lineType=1)
            res_map = res_map * 255
            image = cv2.vconcat([image, res_map.astype(np.uint8)])
            cv2.imwrite(output + image_path.split('/')[-1].split('.')[0] + '_rect.png', image)
            '''

    def gomi_detecter(self, boxes, res_map):
        # print(len(boxes))
        if len(boxes) == 0 or len(boxes) > 2:
            return True
        elif len(boxes) == 2:
            if abs(boxes[1][1] - boxes[0][1]) > 8 and abs(boxes[1][0] - boxes[0][0]) > 8:
                return True
        return False

    def _resize_image_keeping_aspect_ratio(self, img, remove_top_bottom_as_well=True, constant_input_shape=None):
        # FIXME:batch support
        if img.shape[0] * img.shape[1] <= 0:
            return img

        # remove the redundant space on both of right and left side
        gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        thresh = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        x, y, w, h = cv2.boundingRect(np.invert(thresh))
        if remove_top_bottom_as_well:
            if w > 0 and h > 0:
                img = img[y: y + h, x: x + w, :]
        else:
            if w > 0:
                img = img[:, x: x + w, :]

        # change the height to the target one
        if constant_input_shape is None:
            _, input_w, input_h, input_ch = self.model.input_shape
        else:
            _, input_h, input_w, input_ch = constant_input_shape
        img = cv2.resize(img, (max(1, input_h * img.shape[1] // img.shape[0]), input_h),
                         interpolation=cv2.INTER_CUBIC)

        # make the image shrink the width if it is still long
        if input_w < img.shape[1]:
            img = cv2.resize(img, (input_w, input_h),
                                   interpolation=cv2.INTER_CUBIC)

        # otherwise, add blank space to the right so that the size of the image can be the given one
        elif input_w >= img.shape[1]:
            img = self._set_margin(img, margins=(0, input_w - img.shape[1], 0, 0))

        return img

    def _set_margin(self, img, margins=(10, 10, 10, 10), background_color=255):
        canvas = np.zeros((img.shape[0] + margins[0] + margins[2],
                           img.shape[1] + margins[1] + margins[3], img.shape[-1]), np.uint8) + background_color
        y1, y2 = margins[0], margins[0] + img.shape[0]
        x1, x2 = margins[3], margins[3] + img.shape[1]
        canvas[y1: y2, x1: x2, :] = img
        return canvas

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--modelfile', type=str, default='/Users/masayaiwasaki/source/release/OCR_Engine_detection.aiinside')
    parser.add_argument('--image_path', type=str, default='image/')
    args = parser.parse_args()

    modelfile = args.modelfile
    image_path = args.image_path
    recognizer = MultilinesBaseRecognizer(modelfile=modelfile, detection_modelfile=modelfile)
    recognizer.load()
    recognizer.norm(image_path)


