import subprocess
from utils import img_glob
import argparse
import os

parser = argparse.ArgumentParser(description='')
parser.add_argument('--modeltype', type=str, default='address')
parser.add_argument('--modelfile', type=str, default='/Users/masayaiwasaki/source/release/OCR_Engine_address.aiinside')
parser.add_argument('--image_path', type=str, default='image/')
parser.add_argument('--leven_thre', type=int, default=3)
parser.add_argument('--sort_flag', type=bool, default=True)
parser.add_argument('--batch_size', type=int, default=16)
parser.add_argument('--image_num', type=int, default=10000)

args = parser.parse_args()

image_num = args.image_num
image_path_num = len(img_glob(args.image_path))

for start in range(0, image_path_num, image_num):
    p = subprocess.call([
        'python', 'sorting_crnn.py',
        '--modeltype', str(args.modeltype),
        '--modelfile', str(args.modelfile),
        '--image_path', str(args.image_path),
        '--leven_thre', str(args.leven_thre),
        '--batch_size', str(args.batch_size),
        '--sort_flag', str(args.sort_flag),
        '--start', str(start),
        '--end', str(min([start+image_num, image_path_num]))
    ])
