# coding=utf-8
from utils import img_glob
import os
import shutil


def load_label_from_text(img_path_list, encodeing='cp932'):
    img_info_list = []
    for img_path in img_path_list:
        txt_path = img_path.replace('.png', '.txt')
        if os.path.exists(txt_path):
            f = open(txt_path, encoding=encodeing)
            try:
                line = f.readline()
                img_info_list.append({'filename': img_path, 'answer': line.replace('\n', '')})
            except UnicodeDecodeError:
                line = f.readline()
                print(txt_path, 'cannnot read')
    return img_info_list


def load_label_from_text2(img_path_list, encodeing='cp932'):
    img_info_list = []
    for img_path in img_path_list:
        # txt_path = img_path.replace('.png', '.txt')
        txt_path = '_'.join(img_path.split('_')[:-1]) + '.txt'
        # print(txt_path)
        if os.path.exists(txt_path):
            f = open(txt_path, encoding=encodeing)
            try:
                line = f.readline()
                img_info_list.append({'filename': img_path, 'answer': line.replace('\n', '')})
            except UnicodeDecodeError:
                print(txt_path, 'cannnot read')
    return img_info_list


def load_label_from_text3(img_path_list, encodeing='cp932'):
    img_info_list = []
    for img_path in img_path_list:
        # txt_path = img_path.replace('.png', '.txt')
        txt_path = '_'.join(img_path.split('_')[:-1]) + '.txt'
        # print(txt_path)
        if os.path.exists(txt_path):
            f = open(txt_path, encoding=encodeing)
            try:
                line = f.readline()
                img_info_list.append({'filename': img_path, 'answer': line.replace('\n', '')})
            except UnicodeDecodeError:
                print(txt_path, 'cannnot read')
    return img_info_list


def mv_and_rename(img_info_list, output_path):
    for img_info in img_info_list:
        img_name = img_info['filename'].split('/')[-1]
        img_name = output_path + img_name.split('.')[-2] + '_' + img_info['answer'] + '.png'
        print(img_name)
        shutil.move(img_info['filename'], img_name)


def mv_and_rename_price(img_info_list, output_path):
    for img_info in img_info_list:
        img_name = img_info['filename'].split('/')[-1]
        if len(img_info['answer']) >= 4:
            img_name = output_path + img_name.split('.')[-2] + '_' + img_info['answer'] + '.png'
        else:
            img_name = output_path + img_name
        print(img_name)
        shutil.move(img_info['filename'], img_name)


def mv_and_rename_remove2(img_info_list, output_path):
    for img_info in img_info_list:
        img_name = img_info['filename'].split('/')[-1]
        img_name = output_path + img_name.split('.')[0] + img_info['answer'] + '.png'
        print(img_name)
        shutil.move(img_info['filename'], img_name)

def mv_and_rename_back_img(img_path, output_path):
    img_path_list = img_glob(img_path)
    for img_name in img_path_list:
        if img_name.split('_')[-1] == '.png':
            move_img_name = output_path + img_name.split('/')[-1]
            print(move_img_name)
            shutil.move(img_name, move_img_name)




data_type = ['bank/', 'city/', 'kanjiname/', 'bankbranch/', 'datetime/', 'prefecture/'][2]
data_type = 'price_insite1/'  # 'price_insite1/'
img_path = 'insite/temp2/' + data_type
# output_path = '/Users/masayaiwasaki/Downloads/insite_cut_nolabel_sorted/2/' + data_type
output_path = 'rename/' + data_type

os.makedirs(output_path, exist_ok=True)

img_path_list = img_glob(img_path)

# mv_and_rename_back_img(output_path, img_path)
img_info_list = load_label_from_text(img_path_list)
mv_and_rename(img_info_list, output_path)
