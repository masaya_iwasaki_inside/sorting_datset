python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_rename/address0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_rename/address1/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_rename/bank/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_rename/bankbranch/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_rename/city/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_rename/datetime/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_rename/fulladdress0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_rename/fulladdress1/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_rename/kanjiname/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_rename/prefecture/

python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_rename/address0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_rename/address1/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_rename/bank/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_rename/bankbranch/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_rename/city/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_rename/datetime/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_rename/fulladdress0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_rename/fulladdress1/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_rename/kanjiname/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_rename/prefecture/

python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_rename/address0/
python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_rename/address1/
python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_rename/bank/
python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_rename/bankbranch/
python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_rename/city/
python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_rename/datetime/
python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_rename/fulladdress0/
python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_rename/fulladdress1/
python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_rename/kanjiname/
python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_rename/prefecture/

