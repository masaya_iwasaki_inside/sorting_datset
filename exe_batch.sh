python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/bank1/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/bankbranch1/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/bukken0/
# python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/kanjiname0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/katakana0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/katakananame0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/katakananame1/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/longnum2/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/longnum3/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/longnum4/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/phone0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/phone_0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/shortnum0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/shortnum1/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_labeling/shortnum3/

python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/bank1/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/bankbranch1/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/bukken0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/kanjiname0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/katakana0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/katakananame0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/katakananame1/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/longnum2/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/longnum3/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/longnum4/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/phone0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/phone_0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/shortnum0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/shortnum1/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_labeling/shortnum3/

python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_cut_labeling/bank1/
python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_cut_labeling/bankbranch1/
python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_cut_labeling/bukken0/
python run_sorting_crnn.py --modeltype name --modelfile OCR_Engine_name.aiinside --image_path ../insite_cut_labeling/kanjiname0/
python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_cut_labeling/katakana0/
python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_cut_labeling/katakananame0/
python run_sorting_crnn.py --modeltype phone --modelfile OCR_Engine_address.aiinside --image_path ../insite_cut_labeling/katakananame1/
python run_sorting_crnn.py --modeltype phone --modelfile OCR_Engine_phone.aiinside --image_path ../insite_cut_labeling/longnum2/
python run_sorting_crnn.py --modeltype phone --modelfile OCR_Engine_phone.aiinside --image_path ../insite_cut_labeling/longnum3/
python run_sorting_crnn.py --modeltype phone --modelfile OCR_Engine_phone.aiinside --image_path ../insite_cut_labeling/longnum4/
python run_sorting_crnn.py --modeltype phone --modelfile OCR_Engine_phone.aiinside --image_path ../insite_cut_labeling/phone0/
python run_sorting_crnn.py --modeltype phone --modelfile OCR_Engine_phone.aiinside --image_path ../insite_cut_labeling/phone_0/
python run_sorting_crnn.py --modeltype phone --modelfile OCR_Engine_phone.aiinside --image_path ../insite_cut_labeling/shortnum0/
python run_sorting_crnn.py --modeltype phone --modelfile OCR_Engine_phone.aiinside --image_path ../insite_cut_labeling/shortnum1/
python run_sorting_crnn.py --modeltype address --modelfile OCR_Engine_address.aiinside --image_path ../insite_cut_labeling/shortnum3/
