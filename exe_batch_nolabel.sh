python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/address0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/address1/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/bank0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/bankbranch0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/city0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/city1/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/datetime0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/datetime1/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/datetime2/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/fulladdress0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/fulladdress1/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/kanjiname1/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/katakana1/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/prefecture0/
python sorting_detection.py --modelfile OCR_Engine_detection.aiinside --image_path ../insite_cut_nolabel/prefecture1/

python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/address0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/address1/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/bank0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/bankbranch0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/city0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/city1/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/datetime0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/datetime1/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/datetime2/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/fulladdress0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/fulladdress1/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/kanjiname1/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/katakana1/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/prefecture0/
python sorting_blank.py --modelfile OCR_Engine_blank.aiinside --image_path ../insite_cut_nolabel/prefecture1/
