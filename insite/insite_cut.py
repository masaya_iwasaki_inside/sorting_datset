from pdf2image import convert_from_path
import numpy as np
import cv2
import glob
import re
import os
import shutil
import multiprocessing
from multiprocessing import Pool


def pdf_glob(path):
    ext = '.*\.pdf'
    path = os.path.join(path, '*')
    files = glob.glob(path)
    files = [f for f in files if re.search(ext, f, re.IGNORECASE)]
    return files


def find_rect(image):
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    thresh = 255 - cv2.threshold(gray_image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    thresh = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, np.ones((7, 7), np.uint8))
    contours = cv2.findContours(thresh, 1, 2)[1]

    rects = []
    for contour in contours:
        approx = cv2.convexHull(contour)
        rect = cv2.boundingRect(approx)
        rects.append(np.array(rect))
    return rects


def find_rect_of_red(image):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV_FULL)
    h = hsv[:, :, 0]
    s = hsv[:, :, 1]
    mask = np.zeros(h.shape, dtype=np.uint8)
    mask[((h < 20) | (h > 200)) & (s > 128)] = 255
    _, contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    rects = []
    for contour in contours:
        approx = cv2.convexHull(contour)
        rect = cv2.boundingRect(approx)
        rects.append(np.array(rect))
    return rects


def find_rect_of_black(image):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV_FULL)
    s = hsv[:, :, 1]
    v = hsv[:, :, 2]
    mask = np.zeros(s.shape, dtype=np.uint8)
    mask[(v < 150) & (s < 150)] = 255
    _, contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    rects = []
    for contour in contours:
        approx = cv2.convexHull(contour)
        rect = cv2.boundingRect(approx)
        rects.append(np.array(rect))
    return rects


def top_right_finder(img):
    cut_wide = int(3*img.shape[1]/4)
    img = img[:int(img.shape[0]/10), cut_wide:]
    rects = find_rect(img)
    if not len(rects) > 0:
        print('nothing red region')
        return []
    rect = max(rects, key=(lambda x: x[2] * x[3]))
    return (cut_wide+rect[2]+rect[0], rect[1])


def bottom_left_finder(img):
    cut_height = int(9*img.shape[0]/10)
    img = img[cut_height:, :int(img.shape[1]/4)]
    rects = find_rect(img)
    if not len(rects) > 0:
        print('nothing red region')
        return []
    rect = max(rects, key=(lambda x: x[2] * x[3]))
    return (rect[0], cut_height+rect[1]+rect[3])


def bottom_right_finder(img):
    cut_height = int(9*img.shape[0]/10)
    cut_wide = int(3*img.shape[1]/4)
    img = img[cut_height:, cut_wide:]
    rects = find_rect(img)
    if not len(rects) > 0:
        print('nothing red region')
        return []
    rect = max(rects, key=(lambda x: x[2] * x[3]))
    return (cut_wide+rect[0]+rect[2], cut_height+rect[1]+rect[3])


def check_form(img):
    check_region = [(344, 2350), (940, 2396)]
    img = img[check_region[0][1]:check_region[1][1], check_region[0][0]:check_region[1][0]]
    gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    thresh = 255 - cv2.threshold(gray_image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    thresh = thresh / 255
    black_sum = sum(sum(thresh))
    is_type1 = black_sum > 1500
    return is_type1

    # cv2.imwrite('test.png', img)


def cut_parts(pdf_path, points_list1, points_list2, output_path):
    temp = convert_from_path(pdf_path)
    if len(temp) == 0:
        print(pdf_path, 'continue!!')
        return
    img = temp[0]
    img = np.asarray(img)
    img_h, img_w, _ = img.shape
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    top_right = top_right_finder(img)
    bottom_left = bottom_left_finder(img)
    bottom_right = bottom_right_finder(img)
    if len(top_right) == 0 or len(bottom_left) == 0 or len(bottom_right) == 0:
        print('continue')
        return

    pts1 = np.float32([top_right, bottom_left, bottom_right])
    pts2 = np.float32([(img_w, 0), (0, img_h), (img_w, img_h)])
    M = cv2.getAffineTransform(pts1, pts2)
    dst = cv2.warpAffine(img, M, (img_w, img_h + 80))
    dst = cv2.resize(dst, (1654, 2420), interpolation=cv2.INTER_CUBIC)

    is_type1 = check_form(dst)
    if is_type1:
        points_list = points_list1
    else:
        points_list = points_list2

    for type in points_list:
        for (i, p) in enumerate(points_list[type]):
            outpath = output_path + type + str(i) + '/'
            os.makedirs(outpath, exist_ok=True)
            output_name = outpath + os.path.splitext(pdf_path.split('/')[-1])[0] + '_' + type + '.png'
            cv2.imwrite(output_name, dst[p[0][1]:p[1][1], p[0][0]:p[1][0]])

    if is_debug:
        region_img = dst.copy()
        for type in points_list:
            for (i, p) in enumerate(points_list[type]):
                cv2.rectangle(region_img, p[0], p[1], (0, 0, 255), thickness=2)
        output_name = output_path + os.path.splitext(pdf_path.split('/')[-1])[0] + '.png'
        cv2.imwrite(output_name, region_img)
    print(pdf_path, is_type1)


def wrapper(args):
    return cut_parts(*args)


def multi_process(sampleList):
    workers = int(multiprocessing.cpu_count())
    p = Pool(workers)
    output = p.map(wrapper, sampleList)
    p.close()
    return output


# path = '/Volumes/TOSHIBACANVIOCONNECT/Insite/images/test/'
path_list = ['/Volumes/TOSHIBACANVIOCONNECT/Insite/images/2015/','/Volumes/TOSHIBACANVIOCONNECT/Insite/images/2016/',
             '/Volumes/TOSHIBACANVIOCONNECT/Insite/images/2017/','/Volumes/TOSHIBACANVIOCONNECT/Insite/images/2018/']
# path_list = ['/Volumes/TOSHIBACANVIOCONNECT/Insite/images/2016/',
#              '/Volumes/TOSHIBACANVIOCONNECT/Insite/images/2017/','/Volumes/TOSHIBACANVIOCONNECT/Insite/images/2018/']
path_list = ['/Volumes/TOSHIBACANVIOCONNECT/Insite/images/test/']
output_path = 'temp/'
is_debug = False


datetime_point = [[(360, 128), (825, 198)], [(1156, 1140), (1624, 1188)], [(364, 2312), (528, 2376)]]
katakananame_point = [[(360, 208), (825, 247)], [(262, 760), (1290, 810)]]
kanjiname_point = [[(360, 248), (825, 326)], [(190, 810), (1290, 910)]]
phone_point = [[(1210, 218), (1640, 294)]]
prefecture_point = [[(523, 326), (698, 430)], [(532, 1046), (700, 1136)]]
city_point = [[(760, 326), (1018, 430)], [(768, 1046), (1004, 1136)]]
address_point = [[(1078, 326), (1642, 430)], [(1084, 1046), (1648, 1136)]]
shortnum_point = [[(388, 335), (490, 380)], [(388, 380), (515, 422)], [(188, 712), (356, 756)], [(1392, 960), (1548, 1036)]]
longnum_point = [[(466, 712), (896, 756)], [(1270, 1592), (1648, 1660)], [(588, 1232), (896, 1300)], [(588, 1300), (896, 1368)], [(1288, 712), (1648, 764)]]
katakana_point = [[(356, 938), (1256, 994)], [(264, 1660), (1648, 1712)]]
bukken_point = [[(356, 994), (1256, 1046)]]
bank_point = [[(134, 1592), (468, 1660)], [(910, 620), (1226, 684)]]
bankbranch_point = [[(524, 1592), (828, 1660)], [(1288, 620), (1570, 684)]]
kouza_point = [[(188, 1712), (1648, 1784)]]
numalpha_point = [[(1132, 1440), (1648, 1508)]]

fulladdress_point = [[(502, 326), (1642, 430)], [(502, 1046), (1648, 1136)]]
datetime_point2 = [[(360, 128), (825, 198)], [(1156, 1140), (1624, 1188)]]
phone_point2 = [[(1200, 208), (1640, 326)]]
shortnum_point2 = [[(368, 335), (470, 380)], [(368, 380), (495, 422)], [(188, 712), (356, 756)], [(1392, 960), (1548, 1036)]]
longnum_point2 = [[(466, 712), (896, 756)], [(1270, 1592), (1648, 1660)], [(623, 1232), (836, 1300)], [(623, 1300), (836, 1368)], [(1288, 712), (1648, 764)]]

'''
points_list1 = {'datetime': datetime_point, 'katakananame': katakananame_point, 'kanjiname': kanjiname_point,
               'phone': phone_point, 'prefecture': prefecture_point, 'city': city_point, 'address': address_point,
               'shortnum': shortnum_point, 'longnum': longnum_point, 'katakana': katakana_point, 'bukken': bukken_point,
               'bank': bank_point, 'bankbranch': bankbranch_point, 'kouza': kouza_point, 'numalpha': numalpha_point}


points_list2 = {'datetime': datetime_point2, 'katakananame': katakananame_point, 'kanjiname': kanjiname_point,
               'phone_': phone_point2, 'fulladdress': fulladdress_point,
               'shortnum': shortnum_point2, 'longnum': longnum_point2, 'katakana': katakana_point, 'bukken': bukken_point,
               'bank': bank_point, 'bankbranch': bankbranch_point, 'kouza': kouza_point, 'numalpha': numalpha_point}
'''

# longnum_point2 = [[(623, 1232), (836, 1300)], [(623, 1300), (836, 1368)]]

points_list1 = {}
points_list2 = {'phone_': phone_point2}

# if os.path.exists(output_path):
#     shutil.rmtree(output_path)
os.makedirs(output_path, exist_ok=True)
# pdf_path_list = [path+'20180105184418800_0002.pdf']

for path in path_list:
    print(path)
    pdf_path_list = pdf_glob(path)
    args = [[pdf_path, points_list1, points_list2, output_path] for pdf_path in pdf_path_list]
    multi_process(args)
