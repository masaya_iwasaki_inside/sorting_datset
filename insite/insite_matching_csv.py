# -*- coding: utf-8 -*-
from pdf2image import convert_from_path
import numpy as np
import cv2
import glob
import re
import os
import shutil
import multiprocessing
from multiprocessing import Pool


def png_glob(path):
    ext = '.*\.png'
    path = os.path.join(path, '*')
    files = glob.glob(path)
    files = [f for f in files if re.search(ext, f, re.IGNORECASE)]
    return files


def csv_glob(path):
    ext = '.*\.csv'
    path = os.path.join(path, '*')
    files = glob.glob(path)
    files = [f for f in files if re.search(ext, f, re.IGNORECASE)]
    return files


def csv_loader(path):
    info_list = []
    # print(path)
    try:
        for line in open(path, 'r'):
            line = line.split(',')
            if len(line[0]) > 30:
                line = line[0].split('\t')
            print(path, line[0])

            if not (line[0] == '﻿[SN]' or line[0] == '[SN]'):
                line_ = [line[0]]
                for l in line[1:]:
                    line_.append(l.replace('/', '／').replace('_', '＿'))
                line = line_
                i = line[0].index('2')
                # info = {'filename': line[0][i:], 'kanjiname0': line[5], 'katakananame0': line[6], 'bukken0': line[7],
                #         'katakana0': line[8], 'shortnum3': line[9], 'shortnum0': line[12].split('-')[0],
                #         'shortnum1': line[12].split('-')[-1], 'phone_0': line[15], 'phone0': line[15], 'longnum2': line[23],
                #         'longnum3': line[24], 'bank1': line[63], 'bankbranch1': line[66], 'longnum4': line[69], 'katakananame1': line[70]}
                info = {'filename': line[0][i:], 'longnum0': line[23], 'longnum1': line[24]}
                info_list.append(info)
    except UnicodeDecodeError:
        print('pass', path)

    return info_list


def copy_labeling(info_list, cut_list, png_dir_path, output_dir):
    print(info_list)
    for info in info_list:
        for cut in cut_list:
            cut_img_path = png_dir_path + cut + '/' + info['filename'] + '_' + cut[:-1] + '.png'
            print(cut_img_path, os.path.exists(cut_img_path))
            if os.path.exists(cut_img_path):
                if not (len(info[cut]) == 0 or info[cut] == '*' or info[cut] == '****'):
                    output_file_name = output_dir + cut + '/' + info['filename'] + '_' + info[cut] + '.png'
                    shutil.copyfile(cut_img_path, output_file_name)
                else:
                    print(cut_img_path)

def func(csv_file_path, cut_list, png_dir_path, output_dir):
    info_list = csv_loader(csv_file_path)
    copy_labeling(info_list, cut_list, png_dir_path, output_dir)


def wrapper(args):
    return func(*args)


def multi_process(sampleList):
    workers = int(multiprocessing.cpu_count())
    p = Pool(workers)
    output = p.map(wrapper, sampleList)
    p.close()
    return output

csv_path = '/Volumes/TOSHIBACANVIOCONNECT/Insite/results/2018/'
png_dir_path = 'temp/'
output_dir = 'temp2/'
csv_file_path_list = csv_glob(csv_path)

# cut_list = ['kanjiname0', 'katakananame0', 'bukken0', 'katakana0', 'shortnum3', 'shortnum0', 'shortnum1', 'phone_0',
#               'phone0', 'longnum2', 'longnum3', 'bank1', 'bankbranch1', 'longnum4', 'katakananame1']
cut_list = ['longnum0', 'longnum1']

for c in cut_list:
    os.makedirs(output_dir + c, exist_ok=True)

count = 0
# for csv_file_path in csv_file_path_list:
#     print(count, csv_file_path)
#     count += 1

args = []
for csv_file_path in csv_file_path_list:
    args.append([csv_file_path, cut_list, png_dir_path, output_dir])
    info_list = csv_loader(csv_file_path)
    copy_labeling(info_list, cut_list, png_dir_path, output_dir)

# multi_process(args)
